# Trigger Event to Test Integration

In this exercise, you will create a webhook integration with a randomly generated endpoint url from `webhook.site`, then place an order using API requests in `Postman` to trigger the `order.paid` event.

## Placing an order

You must complete the following steps using API requests:

* Create a new integration
* Create a product
* Add the product to your cart
* Create a customer
* Checkout (place an order)
* Authorize payment
* Capture payment

### 1. Create a new integration

* Visit [Webhook.site](http://webhook.site) and make note of the randomly generated url endpoint. Leave the browser tab open so you can see the event payload delivered live.
* In Postman, open the `Create an integration` request under `integrations` folder.
* Replace the contents in the `Body` section with:

```json
{
    "data": {
        "type": "integration",
        "integration_type": "webhook",
        "enabled": true,
        "name": "Serverless Function",
        "description": "Pass data to the function when order is paid/captured",
        "observes": [
            "order.paid"
        ],
        "configuration": {
            "url": "<WEBHOOK.SITE RANDOMLY GENERATED URL>",
            "secret_key": "<MT_WEBHOOK_SECRET>"
        }
    }
}
```

> Don't forget to update the `url` and `secret_key`.
* Select `Send`.

### 2. Create a product

* In Postman, open the `Create a product` request under `products_legacy` folder.
* Make sure you are using a unique `slug` and `sku` and the `currency` is updated to the one configured for your store.
* Select `Send`.
> The ID of the product is saved in `productID` environment variable, so it can be used in other requests.

### 3. Add the product to your cart

* To add the item you created in the last step to your cart, send a request using `Add product to cart` under `carts -> cart_items` folder.

### 4. Create a customer

* In Postman, open the `Create a customer` request under `customers` folder.
* Fill up the details in the body of the request and click `Send`.

> The ID of the customer is saved in `customerID` environment variable, so it can be used in other requests.

### 5. Checkout (place an order)

* In Postman, open the `Checkout as customer` request under `checkout` folder.
* Fill up the details in the `billing_address` and `shipping_address` in the body of the request.
* Select `Send`.

> The ID of the order is saved in `orderID` environment variable, so it can be used in other requests.

### 6. Enable manual gateway

* Open the `Update Manual gateway` request under `gateways` folder.
* Ensure `enabled` is set to `true`.
* Click `Send`. 
* Verify the response.

### 7. Authorize payment

* Now, authorize payment for your order by sending an `Authorize` request under `payments -> manual` folder.

### 8. Capture payment

* To capture payment for your authorized payment, send a request using the `Capture a transaction` under `transactions` folder.

### 9. Verify

You have successfully placed an order and that should have generated the event to trigger the webhook.

* Head back to [Webhook.site](http://webhook.site) and confirm the receipt of the event.

[Next: Sign up for Postmark](signup-for-postmark.md)