## Place an Order to Test the Serverless Application

In this exercise, you will update the webhook integration that you created in the first exercise with the API Gateway endpoint deployed, then place a new order to trigger the `order.paid` event.

You must complete the following steps using API requests in `Postman`:

* Update the integration
* Checkout (place an order)
* Authorize payment
* Capture payment

### 1. Update the integration

* In Postman, open the `Update an integration` request under `integrations` folder.
* Replace the contents in the `Body` section with:

```json
{
  "data": {
    "id": "{{integrationID}}",
    "type": "integration",
    "configuration": {
            "url": "<API GATEWAY ENDPOINT>",
            "secret_key": "<MT_WEBHOOK_SECRET>"
        }
  }
}
```

> Don't forget to update the `url` and `secret_key`.
* Select `Send`.

### 2. Checkout (place an order)

* Open the `Checkout as customer` request under `checkout` folder.

### 3. Authorize payment

* Now, authorize payment for your order by sending an `Authorize` request under `payments -> manual` folder.

### 4. Capture payment

* To capture payment for your authorized payment, send a request using the `Capture a transaction` under `transactions` folder.

### 5. Verify

That’s all! You have successfully placed an order and that should have fired the event to the webhook.

* Confirm the receipt of the order confirmation email.

---

## Troubleshoot

If the email is not delivered, complete these troubleshooting steps:

1. Head back to [Postmark](www.postmarkapp.com) to check the details under `Server > My First Server > Default Transactional Stream > Activity` tab.
2. If there is no activity in `Postmark`, check the logs in `CloudWatch` for Lambda and API Gateway.
3. Get the integration logs. In Postman, send a request using `Get Logs for an Integration` under `Integrations` folder.
4. If you are using Ngrok for local dev, it provides a real-time web UI where you can introspect all of the HTTP traffic running over your tunnels. After you've started ngrok, just open `http://localhost:4040` in a web browser to inspect request details. Also, Ngrok allows you to replay any request. Click the `Replay` button at the top-right corner of any request to replay it.

[Next: Extend the Application](create-and-update-short-order-id.md)