## Develop and Deploy a Serverless Application

In these exercises, you will use Serverless Framework to create, test and deploy a serverless application comprising of AWS Lambda functions and API Gateway.

### Prerequisites

In order to prepare your machine for hands-on exercises, you need to have a few tools installed on your machine. For details, visit [install prerequisites](./install-prerequisites.md) page.

As part of the training, you will sign up for the following services. If you already have an account that you can use for the training, you are free to do that.

* Serverless Framework (SLS) Open Source free account
* Postmark (free trial)


### Objectives

* Set up Postmark account
* Install Serverless Framework Open Source 
* Develop a serverless application/service
* Deploy the serverless application with Serverless Framework
* Test the serverless application with Webhook integration


### Load API collection in Postman

You will be sending the API requests with Postman to complete the exercises, so before you proceed get your API collection ready in the Postman:

* Download the free version of the Postman API tool
* Visit the [EPCC Postman Collection](https://documentation.elasticpath.com/commerce-cloud/docs/developer/how-to/test-with-postman-collection.html) page and download the `Collection` and `Environment`. 
* Import the downloaded collection and environment into Postman.

### Set up the Training Environment

You will be using the API key from your training store to create a new environment in Postman.
>You can request a training store by following the instructions from the [Request a Training Store](https://learn.elasticpath.com/learn/course/internal/view/elearning/11/epcc103-requesting-an-elastic-path-commerce-cloud-training-store) module.

* Visit https://dashboard.elasticpath.com/ and make note of your `Client ID` and `Client secret`.
* In Postman, complete the following steps:

    1. Select the `EPCC Environment` environment from the upper-right hand corner.
    2. Update environment variables named `clientID` and `clientSecret` using the values from the previous step.

As you progress through the collection, additional variables will be created and used from request to request. You can confirm the value of any variable's current value from the environment settings.

>Variables in Postman are in double curly braces `{{}}`.

### Authenticate

* Open the `Get client credentials token` request under `authentication` folder.
* Click `Send`. There is a script that will save the authorization token received back in an `accessToken` variable in Postman.

> Client credentials token is valid for 30 minutes only. You can get a new token following the instructions above if you get a `401` error.

### Exercises

1. [Create an integration](create-integration.md)
2. [Sign up for Postmark and create an email template](./signup-for-postmark.md)
3. [Sign up for an AWS account](./signup-for-aws.md)  
4. [Develop and deploy a serverless application](./setup-serverless-env.md)
5. [Test Serverless Function](./place-an-order.md)
6. [Create a function to update order](./create-and-update-short-order-id.md)
7. [Final test](./final-test.md)