# Sign up and Configure a Postmark Account


In this exercise, you will sign up for a Postmark account, create a new server and create an email template for order confirmation. You will also use curl to test the API with new server and email template.

## Sign up for a free trial with Postmark

> If you already have a Postmark account, skip this step.

* Head to [Postmarkapp](https://postmarkapp.com) and sign up for a free trial.
* Create a `Sender Signature` that will be used to send emails.

---

## Create a server and email template

* Under `Server > My First Server > API Tokens`, note the Server API token from the textbox.
* Go to `Templates` tab and click `Add Template` to add a `Code your own` template.
> If you had created the `Order Confirmation` template as part of another exercise, make note of the template ID and skip this step.
* Choose `Don’t use a layout` and click `continue`.
* Copy and paste the following code to replace everything in the template. _Make sure you are in `Edit` mode._

```html
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title></title>
    <style type="text/css"></style>
  </head>
  <body>
    <p>
      Thank you for your order {{customer_name}}. The total for your order was
      {{order_total}}
    </p>
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Quantity</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        {{#each order_items}}
        <tr>
          <td>{{name}}</td>
          <td>{{quantity}} x {{meta.display_price.with_tax.unit.formatted}}</td>
          <td>{{meta.display_price.with_tax.value.formatted}}</td>
        </tr>
        {{/each}}
      </tbody>
    </table>
  </body>
</html>
```

Once you’ve saved the template, you will need to make a note of the ID of the template:

![Template ID](images/postmark-template.png)

---

## Test the server

* Go to `Server > My First Server > Default Transactional Stream > Setup Instructions`, and click `curl` tab. Copy the command.
* Open a terminal in Visual Studio Code, and run the command to send a test email.
* Confirm the receipt of the test email.

>While your account is in test mode, you can only send emails to recipients from your domain or the domains that you have verified.

>If the test email is not delivered, check the details under `Server > My First Server > Default Transactional Stream > Activity` tab.

[Next: Sign up for an AWS account](./signup-for-aws.md)