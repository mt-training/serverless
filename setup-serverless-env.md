## Serverless Framework Open Source

In this exercise, you will sign up for a Serverless Framework account. You will also be creating and deploying a serverless application using AWS Lambda functions and AWS API Gateway.

Run the following command to sign up for a Serverless Framework free account. _This will open a new browser tab_. You have the option to signup with Google or Github account.

```bash
$ serverless login
```

## Create a Deployment Profile

Deployment profiles are configurations that can be used for deploying an app to different environments. One example of use is setting up configuration variables that can be substituted in the `serverless.yml` file.

* Clone the sample application

```bash
$ sls install -u https://gitlab.com/epcc-training-examples/
serverlessos.git -n postmark-email-service
```

* Switch to the application directory

```bash
$ cd postmark-email-service
```

* Add the following parameters to the `serverless.yml` file
  * `POSTMARK_API_TOKEN`: the Postmark API token for your server
  * `POSTMARK_FROM_ADDRESS`: a sender email address for the order confirmation email
  * `POSTMARK_CONFIRMATION_TEMPLATE_ID`: the *alias* of the email template you created in Postmark.
  * `MT_WEBHOOK_SECRET`: a pre-shared string that will be used when creating an integration.
  
The parameters can be set in the following ways - 
1. Hard-coded:
```bash
$ environment:
    MOLTIN_CLIENT_ID: client1
    MOLTIN_CLIENT_SECRET: secretsecret
```
2. Set in your serverless dashboard and are then specified by interpolation variables:
```bash
$ environment:
    environment:
    MOLTIN_CLIENT_ID: ${param:MOLTIN_CLIENT_ID}
    MOLTIN_CLIENT_SECRET: ${param:MOLTIN_CLIENT_SECRET}
```

### Create a new application

Switch to the application directory and use `npm install` to install dependencies _ignore the warnings_:

```bash
$ cd postmark-email-service
$ npm install
```

Next, create your serverless application ready for monitoring and troubleshooting with the dashboard. When it prompts you for the application name, enter `serverless-email-app`.

```bash
$ serverless
You can monitor, troubleshoot, and test your new service with a free Serverless account.

Serverless: Would you like to enable this? Yes

Serverless: What application do you want to add this to? [create a new app]
Serverless: What do you want to name this application? serverless-email-app

Your project is setup for monitoring, troubleshooting and testing

Deploy your project and monitor, troubleshoot and test it:
- Run “serverless deploy” to deploy your service.
- Run “serverless dashboard” to view the dashboard.
```

When running serverless script it asks for AWS credentials, choose the "AWS Access Role (most secure)". Then select provider type of "access/secret keys”. This will then create a provider inside your serverless profile. The provider is a credential stored for a cloud service.

### Deploy a new service

>Before you deploy your application, if you are using an AWS profile other than `default`, update it in the `provider` section of the `serverless.yml` file.

```bash
$ sls deploy
Serverless: Packaging service...
Serverless: Excluding development dependencies...
Serverless: Installing dependencies for custom CloudFormation resources...
Serverless: Safeguards Processing...
Serverless: Safeguards Results:

   Summary --------------------------------------------------

   passed - allowed-runtimes
   passed - no-unsafe-wildcard-iam-permissions
   passed - framework-version
   passed - no-secret-env-vars
   passed - allowed-regions
   passed - allowed-stages
   warned - require-cfn-role

   Details --------------------------------------------------

   1) Warned - no cfnRole set
      details: http://slss.io/sg-require-cfn-role
      Require the cfnRole option, which specifies a particular role for CloudFormation to assume while deploying.


Serverless: Safeguards Summary: 6 passed, 1 warnings, 0 errors
Serverless: Creating Stack...
Serverless: Checking Stack create progress...
........
Serverless: Stack create finished...
Serverless: Uploading CloudFormation file to S3...
Serverless: Uploading artifacts...
Serverless: Uploading service postmark-email-service.zip file to S3 (6.31 MB)...
Serverless: Uploading custom CloudFormation resources...
Serverless: Validating template...
Serverless: Updating Stack...
Serverless: Checking Stack update progress...
.........................................................
Serverless: Stack update finished...
Service Information
service: postmark-email-service
stage: dev
region: us-east-2
stack: postmark-email-service-dev
resources: 20
api keys:
  None
endpoints:
  POST - https://flru4h6pl2.execute-api.us-east-2.amazonaws.com/dev/webhooks/postmark-email
functions:
  postmark_email: postmark-email-service-dev-postmark_email
layers:
  None
Serverless: Publishing service to the Serverless Dashboard...
Serverless: Successfully published your service to the Serverless Dashboard: https://dashboard.serverless.com/tenants/nageshdixit/applications/serverless-email-app/services/postmark-email-service/stage/dev/region/us-east-2
```

### Verify and check logs

Visit the [AWS console](http://console.aws.amazon.com/) to go over the resources created by the deployment. You should expect new resources created in the following services:

* Lambda
* API Gateway
* S3
* CloudFormation
* IAM

Logs for the Lamda function and for the API Gateway can be found in `CloudWatch`. You can also run the following command in a **new** terminal to tail the logs for your deployed functions:

```bash
$ cd postmark-email-service
$ sls logs -f postmark_email -t
```

The following command can be used to update the Lambda function without redeploying all the other resources:

```bash
sls deploy function -f postmark_email
```

Display information about the deployed service:

```
serverless info
```

###  Local development in offline mode (Optional)

You can also run your serverless applications in offline mode:

```bash
$ sls offline
offline: Starting Offline: dev/us-east-2.
offline: Offline [http for lambda] listening on http://localhost:3002

   ┌──────────────────────────────────────────────────────────────────────────────────┐
   │                                                                                  │
   │   POST | http://localhost:3000/dev/webhooks/postmark-email                       │
   │   POST | http://localhost:3000/2015-03-31/functions/postmark_email/invocations   │
   │                                                                                  │
   └──────────────────────────────────────────────────────────────────────────────────┘

offline: [HTTP] server ready: http://localhost:3000 🚀
offline: 
offline: Enter "rp" to replay the last request
```

>You will need to run `ngrok` to tunnel the local server to the web before you can use it as an endpoint in a Webhook.

[Next: Test Serverless Application with Webhook Integration](./place-an-order.md)