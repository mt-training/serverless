### Installing the Visual Studio Code

* Download and install Visual Studio Code to your local machine
https://code.visualstudio.com/download      

### Installing Git-SCM

* Download and install Git-SCM from [here](https://git-scm.com/download/) selecting all default options.
* Open Visual Studio Code and set default terminal to bash:
> The following steps are required to be completed for Windows only. On Mac and Linux `Bash` is usually the default shell.

* From the top menu item `Terminal` select `New Terminal`. It should open a terminal window in the lower right corner.
* Select the powershell dropdown

![Powershell Dropdown](images/powershell_dropdown.png)

* Then select `Select Default Shell` which will open the list at the top and select `Git Bash`.

![Git BASH](images/git_bash.png)

* Close the existing terminal windows by selecting the trash can in the terminal window header.

![Terminal Close](images/terminal_close.png)

* Open bash terminal from the top navigation menu `Terminal`, `New Terminal`.

### Installing the Node.JS (with npm)

* Download and install [Node.js with npm](https://nodejs.org/en/)

>No need to install all node.js additional tools

### Installing the AWS CLI

* Download and Install awscli from [here](https://aws.amazon.com/cli/)

* Run  `aws configure` to configure the aws-cli and credentials:

```bash
$ aws configure
AWS Access Key ID [None]: AKIAIOSFODAA7EXAMPLE
AWS Secret Access Key [None]: wJalrXUtnAAMI/K7MDENG/bPxRfiCYEXAMPLEKEY
Default region name [None]: us-east-2
Default output format [None]: ENTER
```

> Credentials are stored in INI format in `~/.aws/credentials`. You are provided with an AWS account and credentials in the welcome email, but you can use your own account, provided you have permissions to work with Lambda, API Gateway and S3.

### Installing the Postman (free version)

* Download and install Postman from [here](https://www.postman.com/downloads/)

### Installing the Serverless Framework 

* Install Serverless Framework with npm:

```bash
$ npm install serverless -g
```

