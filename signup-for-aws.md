## Set up AWS Account

In this exercise, you will sign up for an AWS free tier account, create a new user and set up programmatic access.

### Sign up for an AWS account (free tier)

>If you already have an AWS account, skip this step and simply create a new IAM User with programmatic access to be used by Serverless.

To create an AWS account:

* Open [Amazon Web Service](https://aws.amazon.com/), and then choose `Create a Free Account`.
* Follow the online instructions.
* Part of the sign-up procedure involves receiving a phone call and entering a PIN using the phone keypad.

>Note your AWS account ID, because you'll need it for the next task.

If you're new to Amazon Web Services, make sure you put in a credit card. If you don't have a credit card set up, you may not be able to deploy your resources and you may run into this error:

```text
AWS Access Key Id needs a subscription for the service
```

### Creating IAM User / Access Keys

Follow these steps to create an IAM user for the Serverless Framework:

* Login to your AWS account and go to the Identity & Access Management (IAM) page.

* Click on `Users` and then `Add user`. Enter a name in the first field to remind you this User is related to the Serverless Framework, like `serverless-admin`. 
    * Enable `Programmatic access` by clicking the checkbox. Click Next to go through to the Permissions page. 
    * Click on Attach existing policies directly. Search for and select `AdministratorAccess` then click `Next: Review`.
    * Check to make sure everything looks good and click `Create user`.

>View and copy the API Key & Secret to a secure place. You'll need it in the next step.

> Note that the above steps grant the Serverless Framework administrative access to your account. While this makes things simple for training purposes, we recommend that you create and use more fine grained permissions in production environment.


[Next: Create a Serverless Function](./setup-serverless-env.md)