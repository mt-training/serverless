## Update Order with Short Order ID

In this exercise, you will:

* Create a new Flow to add a Flow field `short_id` to the orders.
* Add a new Lambda function that will create and update the short order id field
* Create a new Amazon SNS topic that will trigger the new function
* Modify the existing function to publish to the SNS topic
* Configure and redeploy the application
* Place an order and verify/troubleshoot the application

### Configuring Flows

Commerce Cloud provides you the ability to extend core resources with the Flows feature. 

* Go to the [Dashboard](https://dashboard.elasticpath.com/app/settings/flows) and navigate to `Settings > Flows`.
* You will want to create a new Flow (or edit if it already exists) for extending Orders. Give it a name and description you will recognise, but **make sure** the `slug` is set to `orders`.
* Next you will want to create a new Field for the Flow you just created. Give the new Field a name and description you will recognise. 
>Make sure the `slug` is set to `short_id` as this is what the serverless function expects.
* Save this Field

### Adding a new Lambda function

Let's add a new function to create a short order id and update the order in the Commerce Cloud with it:

* Create a new file `functions/update.js` with the following code:

```
const { MoltinClient } = require('@moltin/request')
const cuid = require('cuid')
const config = require("./lib/config.js");

const moltin = new MoltinClient({
    client_id: config.MOLTIN_CLIENT_ID,
    client_secret: config.MOLTIN_CLIENT_SECRET
  })
  
module.exports.update_epcc = async(event, context) => {
    
    var id = event.Records[0].Sns.Message;
    console.log('Message received from SNS for order ID:', id);
    try {
        const short_id = cuid.slug().toUpperCase()

        await moltin.put(`orders/${id}`, {
            type: 'order',
            short_id,
        })
        return {
            statusCode: 200,
            body: JSON.stringify({Message: "Updated the order successfully."})
        };
    } catch (errors) {
        console.log(errors);
        return {
            statusCode: 500,
            body: JSON.stringify({Message: "Failed to update the order."})
        };
    }

};
```

### Updating the serverless application

You must add the function to the list of functions in the serverless application and configure it to be triggered when a new message is published to the SNS topic.

#### Adding the function

Add the function that you created in the previous step to the functions section in the `serverless.yml` file:

```
  update_epcc:
    handler: functions/update.update_epcc
    events:
      - sns:
         arn: !Ref SNSTopic
         topicName: UpdateShortOrderID
```

#### Creating the SNS topic

Add a new `resources` section in the `serverless.yml` file to create an Amazon SNS topic.

```
# you can add resources that your application needs here
resources:
  Resources:
    SNSTopic:
      Type: AWS::SNS::Topic
      Properties:
        TopicName: UpdateShortOrderID
```

#### Adding permissions

You will also need to modify the Lambda role to add the required permissions for the Lambda functions to be able to publish to the SNS topic. Add the following in the `provider` section of the `serverless.yml` file:

```
# you can add statements to the Lambda function's IAM Role here
  iamRoleStatements:
    - Effect: "Allow"
      Action: "SNS:Publish"
      Resource: !Ref SNSTopic
```

#### Updating the environment variables

Now, you must add the SNS topic and the store credentials to the environment configuration.

* Add the following to the environment section of the `serverless.yml` file:

```
    MOLTIN_CLIENT_ID: ${param:MOLTIN_CLIENT_ID}
    MOLTIN_CLIENT_SECRET: ${param:MOLTIN_CLIENT_SECRET}
    SNS_ARN: !Ref SNSTopic
```

### Updating the config parameters

* Next, add the following parameters to the `functions/lib/config.js` file:

```
module.exports.MOLTIN_CLIENT_ID = process.env.MOLTIN_CLIENT_ID;
module.exports.MOLTIN_CLIENT_SECRET = process.env.MOLTIN_CLIENT_SECRET;
module.exports.SNS_ARN = process.env.SNS_ARN;
```

### Updating the deployment profile

You have to add the store credentials to your deployment profile in the Serverless Framework:

* On the dashboard, click on your user name (right top) and select `org settings`
* Go to `Deployment Profiles`
* Select your profile and add the following parameters under `parameters` tab:
  * `MOLTIN_CLIENT_ID`: client ID for the store
  * `MOLTIN_CLIENT_SECRET`: client secret for the store
  * `SNS_ARN`: arn for the SNS topic

### Modifying the function

Update the function to publish a new message to the Amazon SNS topic containing the order ID.

Start by adding `aws-sdk` in the require list. Insert the following as the very first line in the file `functions/handler.js`:

```
const AWS = require("aws-sdk");
```

Add the following right before the return statement at the end of the function `postmark_email`:

```
//publish a message to the SNS topic
    console.log("Publishing event to SNS");
    var sns = new AWS.SNS();
    await sns.publish({
        Message: resource.data.id, 
        TopicArn: config.SNS_ARN
        }, async function(err, data) {
        if (err) {
            console.error(err, err.stack);
            return {
                statusCode: 500,
                body: event.body
            };
        }
        console.log("MessageID " + data.MessageId);
    }).promise();
    console.log("Message published");
```

### Redeploying the application

Head back to the terminal and switch to the application directory.

Use `npm install` to install `Commerce Cloud SDK` and `cuid`:

```bash
$ npm install --save @moltin/request
$ npm install --save cuid
```

Deploy the application again:

```
$ sls deploy
```

### Verify and check logs

Visit the [AWS console](http://console.aws.amazon.com/) to go over the resources created by the deployment. You should expect new resources created in the following services:

* SNS
* Lambda
* S3
* CloudFormation
* IAM

Logs for the Lamda function and for the API Gateway can be found in `CloudWatch`. You can also run the following command in a **new** terminal to tail the logs for your deployed functions:

```bash
$ sls logs -f update_epcc -t
```

>The command `sls deploy function -f update_epcc` can be used to update the Lambda function without redeploying all the other resources.

Display information about the deployed service:

```
serverless info
```

[Next: Final Test](./final-test.md)