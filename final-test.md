## Test Serverless Application

In this exercise, you will place a new order to trigger the `order.paid` event and verify the gets updated with short order id.

You must complete the following steps using API requests in `Postman`:

* Checkout (place an order)
* Authorize payment
* Capture payment
* Verify order

### 1. Checkout (place an order)

* Open the `Checkout as customer` request under `checkout` folder.

### 2. Authorize payment

* Now, authorize payment for your order by sending an `Authorize` request under `payments/manual` folder.

### 3. Capture payment

* To capture payment for your authorized payment, send a request using the `Capture` under `payments/manual` folder.

### 4. Verify order

* Open the `Get an order` request under `orders` folder.
* Verify that your order has the `short_id` field updated.
> You can verify this from the `Dashboard -> Orders` as well.
* Also, make sure that you received the order confirmation email.

---

## Troubleshoot

If the `short_id` field is not updated, complete these troubleshooting steps:

1. Get the integration logs. In Postman, send a request using `Get integration logs` under `integrations` folder.
2. Make sure the flow field you created is called `short_id`.
3. Check the `CloudWatch` logs for any errors.
4. Try to publish a message to the SNS topic:
    * Head to the SNS service in the AWS console
    * Open the topic `UpdateShortOrderID`
    * From the top-right corner, choose `Publish message`
    * Copy/paste the order id for the recently placed order and select `Publish message`
---

## Remove the deployment

Removing a deployment is as simple as running the following command from the repository folder:

```bash
$ sls remove
```

---